import 'package:campuswaymobile/providers/user_manager.dart';
import 'package:campuswaymobile/utilities/routes.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class LoginScreen extends StatelessWidget {
  final TextEditingController emailController = TextEditingController();
  final TextEditingController passwordController = TextEditingController();
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async => false,
      child: Consumer<UserManager>(
        builder: (context, userManager, child) {
          return Scaffold(
            key: _scaffoldKey,
            backgroundColor: Theme.of(context).primaryColor,
            body: Center(
              child: SingleChildScrollView(
                child: Card(
                  elevation: 6,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(15.0),
                  ),
                  margin: EdgeInsets.all(13),
                  child: Form(
                    key: _formKey,
                    child: ListView(
                      padding: EdgeInsets.all(18),
                      shrinkWrap: true,
                      children: [
                        Container(
                          width: MediaQuery.of(context).size.width / 5.6,
                          height: MediaQuery.of(context).size.width / 5.6,
                          padding: EdgeInsets.all(20),
                          alignment: Alignment.center,
                          decoration: BoxDecoration(
                            image: DecorationImage(
                              image:
                                  AssetImage('assets/images/logo-background.png'),
                              fit: BoxFit.scaleDown,
                            ),
                          ),
                        ),
                        SizedBox(
                          height: 15,
                        ),
                        TextFormField(
                          enabled: !userManager.loading,
                          controller: emailController,
                          autocorrect: false,
                          keyboardType: TextInputType.emailAddress,
                          decoration: InputDecoration(
                            contentPadding: EdgeInsets.symmetric(
                                vertical: 2.0, horizontal: 17.0),
                            hintText: "Email",
                            fillColor: Colors.white,
                            border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(25.0),
                              borderSide: BorderSide(),
                            ),
                          ),
                          validator: (email) {
                            if (email.isEmpty)
                              return "Email não pode ser vazio";
                            else
                              return null;
                          },
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        TextFormField(
                          enabled: !userManager.loading,
                          controller: passwordController,
                          decoration: InputDecoration(
                            contentPadding: EdgeInsets.symmetric(
                                vertical: 2.0, horizontal: 17.0),
                            hintText: "Senha",
                            fillColor: Colors.white,
                            border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(25.0),
                            ),
                          ),
                          obscureText: true,
                          autocorrect: false,
                          validator: (password) {
                            if (password.isEmpty)
                              return "Senha não pode ser vazia";
                            else if (password.length < 6)
                              return "Senha muito pequena";
                            else
                              return null;
                          },
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: [
                            FlatButton(
                              onPressed: !userManager.loading ? () {} : null,
                              child: Text(
                                "Esqueceu sua senha?",
                                style: TextStyle(
                                    color: Theme.of(context).primaryColor,
                                    fontWeight: FontWeight.bold),
                              ),
                            ),
                          ],
                        ),
                        SizedBox(
                          height: 15,
                        ),
                        ClipRRect(
                          borderRadius: BorderRadius.all(Radius.circular(25)),
                          child: Container(
                            height: 45,
                            child: RaisedButton(
                              onPressed: !userManager.loading
                                  ? () {
                                      if (_formKey.currentState.validate()) {
                                        userManager.login(
                                            login: emailController.text,
                                            password: passwordController.text,
                                            onFail: (error) {
                                              _scaffoldKey.currentState
                                                  .showSnackBar(
                                                SnackBar(
                                                  backgroundColor:
                                                      Colors.redAccent,
                                                  content: Text(
                                                      'Erro no login: $error'),
                                                ),
                                              );
                                            },
                                            onSuccess: () => {
                                                  Navigator.of(context).pushReplacementNamed('/base')
                                                });
                                      }
                                    }
                                  : null,
                              color: Theme.of(context).accentColor,
                              disabledColor:
                                  Theme.of(context).accentColor.withAlpha(100),
                              textColor: Colors.white,
                              child: Text(
                                "Entrar",
                                style: TextStyle(fontSize: 16),
                              ),
                            ),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(top: 16.0, bottom: 5),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Text("Não tem uma conta?"),
                              SizedBox(width: 5),
                              InkWell(
                                onTap: !userManager.loading
                                    ? () => Navigator.of(context)
                                        .pushNamed(RouteNames.register)
                                    : null,
                                child: Text(
                                  "Registre-se",
                                  style: TextStyle(
                                      color: Theme.of(context).primaryColor,
                                      fontWeight: FontWeight.bold),
                                ),
                              )
                            ],
                          ),
                        )
                      ],
                    ),
                  ),
                ),
              ),
            ),
          );
        },
      ),
    );
  }
}
