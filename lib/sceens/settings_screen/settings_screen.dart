
import 'package:campuswaymobile/helpers/shared_preferences/shared_preferences_helper.dart';
import 'package:campuswaymobile/utilities/routes.dart';
import 'package:flutter/material.dart';
import 'package:settings_ui/settings_ui.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SettingsScreeen extends StatelessWidget {

  SharedPreferenceHelper prefs;

  @override
  Widget build(BuildContext context) {

    Future<SharedPreferences> getSharedPref() async{
      return  await SharedPreferences.getInstance();
    }

    prefs = SharedPreferenceHelper(getSharedPref());

    return Scaffold(

      backgroundColor: Colors.grey[200],
      appBar: AppBar(
        leading: Container(),
        centerTitle: true,
        title: Text(
          "Ajustes",
          style: TextStyle(color: Colors.white),
        ),
      ),
      body: SettingsList(
        backgroundColor: Colors.white,
        shrinkWrap: true,
        sections: [
          CustomSection(
            child: SizedBox(
              height: 10,
              child: Container(
                color: Colors.grey[200],
              ),
            ),
          ),
          SettingsSection(
            tiles: [
              SettingsTile(
                title: 'Idioma',
                subtitle: 'Português',
                leading: Icon(Icons.language),
                onTap: () {},
              ),
              SettingsTile(

                title: 'Notificações',
                trailing: IconButton(
                    icon: Icon(Icons.keyboard_arrow_right), onPressed: () {}),
                leading: Icon(Icons.notifications),
                onTap: () {},
                subtitle: 'Alertas e sons',
              ),

              SettingsTile(
                title: 'Sair',
                leading: Icon(Icons.exit_to_app),
                trailing: IconButton(
                    icon: Icon(Icons.keyboard_arrow_right),
                    onPressed: () {
                      showDialog(
                        context:  context,
                        builder:  (BuildContext context) {
                          return AlertDialog(
                            title: Text("Sair"),
                            content: Text("Você deseja sair da sua conta?"),
                            actions: <Widget>[

                              FlatButton(
                                child: new Text("Voltar"),
                                onPressed: () {
                                  Navigator.of(context).pop();
                                },
                              ),
                              FlatButton(
                                child: new Text("Sair"),
                                onPressed: () {
                                  Navigator.of(context).pop();
                                  prefs.logOut();
                                  Navigator.of(context).pushReplacementNamed(RouteNames.login);
                                },
                              ),
                            ],
                          );
                        },
                      );
                    }),

              ),
            ],
          ),
          CustomSection(
            child: SizedBox(
              height: 10,
              child: Container(
                color: Colors.grey[200],
              ),
            ),
          ),
          SettingsSection(
            tiles: [
              SettingsTile(
                title: 'Contato',
                subtitle: 'campusway@unicamp.br',
                leading: Icon(Icons.email),
                onTap: () {},
              ),
            ],
          ),
          CustomSection(
            child: SizedBox(
              height: 10,
              child: Container(
                color: Colors.grey[200],
              ),
            ),
          ),
          SettingsSection(
            tiles: [
              SettingsTile(
                title: 'Políticas de Uso',
                trailing: IconButton(
                    icon: Icon(Icons.keyboard_arrow_right), onPressed: () {}),
                leading: Icon(Icons.supervised_user_circle),
                onTap: () {},
              ),
            ],
          ),
          CustomSection(
            child: Container(
              color: Colors.grey[200],
              child: Column(
                children: [
                  SizedBox(
                    height: 20,
                  ),
                  Text(
                    'Version: 1.0',
                    style: TextStyle(color: Color(0xFF777777)),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                ],
              ),
            ),
          )
        ],
      ),
    );
  }
}
