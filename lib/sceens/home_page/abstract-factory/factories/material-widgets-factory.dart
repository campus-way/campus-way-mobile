

import 'package:campuswaymobile/sceens/home_page/abstract-factory/widgets/iinfos-dialog.dart';
import 'package:campuswaymobile/sceens/home_page/abstract-factory/widgets/info-dialog/android-info-dialog.dart';

import '../iwidgets-factory.dart';

class MaterialWidgetsFactory implements IWidgetsFactory {
  
  @override
  IInfosDialog createInfosDialog() {
    return AndroidInfosDialog();
  }

  
}


