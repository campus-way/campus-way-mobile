import 'package:campuswaymobile/sceens/home_page/abstract-factory/widgets/iinfos-dialog.dart';
import 'package:campuswaymobile/sceens/home_page/abstract-factory/widgets/info-dialog/ios-info-dialog.dart';

import '../iwidgets-factory.dart';

class CupertinoWidgetsFactory implements IWidgetsFactory {
  
  @override
  IInfosDialog createInfosDialog() {
    return IosInfosDialog();
  }
}
