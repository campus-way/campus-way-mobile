import 'package:campuswaymobile/sceens/home_page/abstract-factory/widgets/iinfos-dialog.dart';
import 'package:flutter/material.dart';

class AndroidInfosDialog implements IInfosDialog {
  
  @override
  Future<Widget> render(context,message) {
    return showDialog(
      context: context,
      builder: (_) => new AlertDialog(
        title: new Text("Mensagem"),
        content: new Text(message),
        actions: <Widget>[
          FlatButton(
            child: Text('Fechar'),
            onPressed: () {
              Navigator.of(context).pop();
              Navigator.of(context).pop();
            },
          )
        ],
      ),
    );
  }
}
