import 'widgets/iinfos-dialog.dart';

abstract class IWidgetsFactory {
  
  /// Aqui adicionamos todos os Widgets que iremos criar dentro do App que 
  /// queremos diferenciar de a acordo com o OS (Android x iOS)

  IInfosDialog createInfosDialog();
  
}




