
import 'package:campuswaymobile/models/group.dart';
import 'package:campuswaymobile/sceens/home_page/components/icon_bundle.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class GroupCard extends StatelessWidget {
  GroupCard({this.group, this.onClick});
  final Group group;

  final Function onClick;

  @override
  Widget build(BuildContext context) {
    return Card(
      margin: EdgeInsets.symmetric(horizontal: 8, vertical: 4),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(4),
      ),
      child: Container(
        margin: EdgeInsets.all(15),
        //height: MediaQuery.of(context).size.height / 12.5,
        width: double.infinity,
        child: Row(
          children: [
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: NavBarIcon(icon: Icon(Icons.person,size: 35,),counter: group.participantes,),
            ),
            
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text("Origem: " + group.origem, style: TextStyle(fontSize: 16,fontWeight: FontWeight.w500),),
                Text("Destino: " + group.destino),
                Text("Saindo às " + DateFormat("HH:mm").format(group.horario)+" do dia " +DateFormat("dd/MM/yyyy").format(group.horario)),
              ],
            ),
            
          ],
        ),
      ),
    );
  }
}

