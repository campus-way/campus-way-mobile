import 'package:campuswaymobile/models/group.dart';
import 'package:campuswaymobile/providers/groups_manager.dart';
import 'package:date_format/date_format.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class GroupForm extends StatefulWidget {
  const GroupForm({Key key, this.groupsManager, this.onSuccessCallback})
      : super(key: key);
  final GroupsManager groupsManager;
  final Function onSuccessCallback;

  @override
  _GroupFormState createState() => _GroupFormState();
}

class _GroupFormState extends State<GroupForm> {
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  final TextEditingController _originController = TextEditingController();
  final TextEditingController _destinyController = TextEditingController();
  final TextEditingController _destinyAdressController =
      TextEditingController();
  final TextEditingController _originAdressController = TextEditingController();
  final TextEditingController _timeController = TextEditingController();
  final TextEditingController _dateController = TextEditingController();
  final TextEditingController _referencePointController =
      TextEditingController();
  final _formKey = GlobalKey<FormState>();

  String dateTime;
  String _setTime, _setDate;

  DateTime selectedDate = DateTime.now();
  TimeOfDay selectedTime = TimeOfDay.now();
  String _hour;
  String _minute;
  String _time;

  

  Future<Null> _selectTime(BuildContext context) async {
    final TimeOfDay picked = await showTimePicker(
        context: context,
        initialTime: selectedTime,
        builder: (BuildContext context, Widget child) {
          return Theme(
            data: ThemeData.light().copyWith(
              colorScheme:
                  ColorScheme.light(primary: Theme.of(context).primaryColor),
              buttonTheme: ButtonThemeData(textTheme: ButtonTextTheme.primary),
            ),
            child: child,
          );
        });
    if (picked != null)
      setState(() {
        selectedTime = picked;
        _hour = selectedTime.hour.toString();
        _minute = selectedTime.minute.toString();
        _time = _hour + ' : ' + _minute;
        _timeController.text = _time;
        _timeController.text = formatDate(
            DateTime(2019, 08, 1, selectedTime.hour, selectedTime.minute),
            [hh, ':', nn, " ", am]).toString();
      });
  }

  Future<Null> _selectDate(BuildContext context) async {
    final DateTime picked = await showDatePicker(
        context: context,
        initialDate: selectedDate,
        initialDatePickerMode: DatePickerMode.day,
        firstDate: DateTime(2021),
        lastDate: DateTime(2101),
        builder: (BuildContext context, Widget child) {
          return Theme(
            data: ThemeData.light().copyWith(
              colorScheme:
                  ColorScheme.light(primary: Theme.of(context).primaryColor),
              buttonTheme: ButtonThemeData(textTheme: ButtonTextTheme.primary),
            ),
            child: child,
          );
        });
    if (picked != null)
      setState(() {
        selectedDate = picked;
        _dateController.text = DateFormat.yMd().format(selectedDate);
      });
  }

  @override
  void initState() {
    _dateController.text = DateFormat.yMd().format(DateTime.now());

    _timeController.text = formatDate(
        DateTime(2019, 08, 1, DateTime.now().hour, DateTime.now().minute),
        [hh, ':', nn, " ", am]).toString();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    dateTime = DateFormat.yMd().format(DateTime.now());

    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        title: Text("Novo Grupo"),
      ),
      body: Form(
        key: _formKey,
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.only(top: 12.0, left: 8),
                child: Text(
                  "Data e Hora da saída",
                  textAlign: TextAlign.end,
                  style: TextStyle(color: Colors.black54),
                ),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  GestureDetector(
                    onTap: () {
                      _selectDate(context);
                    },
                    child: Container(
                      width: MediaQuery.of(context).size.width / 2,
                      padding: EdgeInsets.all(8.0),
                      child: TextFormField(
                        enabled: false,
                        keyboardType: TextInputType.text,
                        controller: _dateController,
                        onSaved: (String val) {
                          _setDate = val;
                        },
                        decoration: InputDecoration(
                          contentPadding: EdgeInsets.symmetric(
                              vertical: 2.0, horizontal: 10.0),
                          hintText: "Data",
                          fillColor: Colors.white,
                        ),
                      ),
                    ),
                  ),
                  GestureDetector(
                    onTap: () {
                      _selectTime(context);
                    },
                    child: Container(
                      width: MediaQuery.of(context).size.width / 2,
                      padding: EdgeInsets.all(8.0),
                      child: TextFormField(
                        enabled: false,
                        keyboardType: TextInputType.text,
                        controller: _timeController,
                        onSaved: (String val) {
                          _setTime = val;
                        },
                        decoration: InputDecoration(
                          contentPadding: EdgeInsets.symmetric(
                              vertical: 2.0, horizontal: 10.0),
                          hintText: "Hora",
                          fillColor: Colors.white,
                        ),
                      ),
                    ),
                  ),
                ],
              ),
              Padding(
                padding: EdgeInsets.all(8.0),
                child: TextFormField(
                  controller: _originController,
                  decoration: InputDecoration(
                    contentPadding:
                        EdgeInsets.symmetric(vertical: 2.0, horizontal: 10.0),
                    hintText: "Origem",
                    fillColor: Colors.white,
                  ),
                  validator: (or) {
                    if (or.isEmpty)
                      return "Origem não pode ser vazia";
                    else
                      return null;
                  },
                ),
              ),
              Padding(
                padding: EdgeInsets.all(8.0),
                child: TextFormField(
                  controller: _originAdressController,
                  decoration: InputDecoration(
                    contentPadding:
                        EdgeInsets.symmetric(vertical: 2.0, horizontal: 10.0),
                    hintText: "Endereço origem",
                    fillColor: Colors.white,
                  ),
                  validator: (eor) {
                    if (eor.isEmpty)
                      return "Endereço de origem não pode ser vazio";
                    else
                      return null;
                  },
                ),
              ),
              Padding(
                padding: EdgeInsets.all(8.0),
                child: TextFormField(
                  controller: _destinyController,
                  decoration: InputDecoration(
                    contentPadding:
                        EdgeInsets.symmetric(vertical: 2.0, horizontal: 10.0),
                    hintText: "Destino",
                    fillColor: Colors.white,
                  ),
                  validator: (d) {
                    if (d.isEmpty)
                      return "Destino não pode ser vazio";
                    else
                      return null;
                  },
                ),
              ),
              Padding(
                padding: EdgeInsets.all(8.0),
                child: TextFormField(
                  controller: _destinyAdressController,
                  decoration: InputDecoration(
                    contentPadding:
                        EdgeInsets.symmetric(vertical: 2.0, horizontal: 10.0),
                    hintText: "Endereço destino",
                    fillColor: Colors.white,
                  ),
                  validator: (ed) {
                    if (ed.isEmpty)
                      return "Endereço de destino não pode ser vazio";
                    else
                      return null;
                  },
                ),
              ),
              Padding(
                padding: EdgeInsets.all(8.0),
                child: TextFormField(
                  controller: _referencePointController,
                  decoration: InputDecoration(
                    contentPadding:
                        EdgeInsets.symmetric(vertical: 2.0, horizontal: 10.0),
                    hintText: "Ponto de referência",
                    fillColor: Colors.white,
                  ),
                  validator: (pf) {
                    if (pf.isEmpty)
                      return "Ponto de referência não pode ser vazio";
                    else
                      return null;
                  },
                ),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  Padding(
                    padding: const EdgeInsets.all(12.0),
                    child: RaisedButton(
                      color: Theme.of(context).accentColor,
                      child: Text(
                        "Criar grupo",
                        style: TextStyle(color: Colors.white),
                      ),
                      onPressed: () {
                        if (_formKey.currentState.validate()) {
                          _formKey.currentState.save();

                          DateTime finalDate = DateTime(
                              selectedDate.year,
                              selectedDate.month,
                              selectedDate.day,
                              selectedTime.hour,
                              selectedTime.minute);
                          var group = Group(
                              origem: _originController.text,
                              enderecoOrigem: _originAdressController.text,
                              destino: _destinyController.text,
                              enderecoDestino: _destinyAdressController.text,
                              pontoReferencia: _referencePointController.text,
                              horario: finalDate);
                          widget.groupsManager.createGroup(
                            group: group,
                            onFail: (error) {
                              _scaffoldKey.currentState.showSnackBar(
                                SnackBar(
                                  backgroundColor: Colors.redAccent,
                                  content: Text('Erro ao criar grup: $error'),
                                ),
                              );
                            },
                            onSuccess: () async {
                              // _scaffoldKey.currentState.showSnackBar(
                              //   SnackBar(
                              //     backgroundColor: Colors.blueAccent,
                              //     content: Text('Sucesso!'),
                              //   ),
                              // );
                              this.widget.onSuccessCallback();
                              
                            },
                          );
                        }
                      },
                    ),
                  ),
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}
