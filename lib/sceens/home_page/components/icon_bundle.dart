import 'package:flutter/material.dart';

class NavBarIcon extends StatelessWidget {
  const NavBarIcon({Key key, this.counter, this.icon}) : super(key: key);
  final int counter;
  final Icon icon;
  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        icon,
         Positioned(
          right: 0,
          top: 0,
          child:  Container(
            padding: EdgeInsets.all(1),
            decoration:  BoxDecoration(
              color: Colors.red,
              borderRadius: BorderRadius.circular(6),
            ),
            constraints: BoxConstraints(
              minWidth: 12,
              minHeight: 12,
            ),
            child:  Text(
              '$counter',
              style:  TextStyle(
                color: Colors.white,
                fontSize: 10,
              ),
              textAlign: TextAlign.center,
            ),
          ),
        )
      ],
    );
  }
}