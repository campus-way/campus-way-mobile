import 'package:intl/intl.dart';
import 'dart:io';

import 'package:campuswaymobile/helpers/shared_preferences/dtos.dart';
import 'package:campuswaymobile/models/group.dart';
import 'package:campuswaymobile/providers/groups_manager.dart';
import 'package:campuswaymobile/sceens/home_page/abstract-factory/factories/cupertino-widgets-factory.dart';
import 'package:campuswaymobile/sceens/home_page/abstract-factory/factories/material-widgets-factory.dart';
import 'package:campuswaymobile/sceens/home_page/abstract-factory/iwidgets-factory.dart';
import 'package:campuswaymobile/sceens/home_page/components/group_card.dart';
import 'package:campuswaymobile/utilities/routes.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage>
    with AutomaticKeepAliveClientMixin {
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  IWidgetsFactory widgetsFactory;

  @override
  void initState() {
    super.initState();
    if (Platform.isAndroid) {
      // Android-specific code
      widgetsFactory = MaterialWidgetsFactory();
    } else if (Platform.isIOS) {
      // iOS-specific code
      widgetsFactory = CupertinoWidgetsFactory();
    }
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return Consumer<GroupsManager>(
      builder: (context, groupsManager, child) {
        return DefaultTabController(
          length: 2,
          child: Scaffold(
            key: _scaffoldKey,
            appBar: AppBar(
              leading: Container(),
              centerTitle: true,
              title: Text("CampusWay"),
              bottom: TabBar(
                tabs: <Widget>[
                  Tab(
                    child: Text("Todos"),
                  ),
                  Tab(
                    child: Text("Meus"),
                  )
                ],
              ),
            ),
            body: TabBarView(
              children: <Widget>[
                AllGroupsTab(
                  groupsManager: groupsManager,
                  scaffoldKey: _scaffoldKey,
                  isAll: true,
                ),
                AllGroupsTab(
                  groupsManager: groupsManager,
                  scaffoldKey: _scaffoldKey,
                  isAll: false,
                ),
              ],
            ),
            floatingActionButton: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                FloatingActionButton(
                  heroTag: "btn1",
                  onPressed: () {},
                  child: Icon(
                    Icons.search,
                    color: Colors.white,
                  ),
                ),
                SizedBox(
                  height: 15,
                ),
                FloatingActionButton(
                  heroTag: "btn2",
                  onPressed: () {
                    Navigator.of(context).pushNamed(
                      RouteNames.groupForm,
                      arguments: FormsParams(
                        groupsManager: groupsManager,
                        callBack: () {
                          setState(() {});
                          widgetsFactory
                              .createInfosDialog()
                              .render(context, "Grupo com sucesso!");
                        },
                      ),
                    );
                  },
                  child: Icon(
                    Icons.add,
                    color: Colors.white,
                  ),
                ),
              ],
            ),
          ),
        );
      },
    );
  }

  @override
  // TODO: implement wantKeepAlive
  bool get wantKeepAlive => true;
}

class AllGroupsTab extends StatelessWidget {
  const AllGroupsTab(
      {Key key, this.groupsManager, this.scaffoldKey, this.isAll})
      : super(key: key);
  final GroupsManager groupsManager;
  final GlobalKey<ScaffoldState> scaffoldKey;
  final bool isAll;

  showAlertDialog(
      BuildContext context, GroupsManager groupManager, Group group) {
    final df = DateFormat('dd/MM/yyyy HH:mm ');

    // set up the buttons
    Widget cancelButton = FlatButton(
      child: Text("Cancelar"),
      onPressed: () {
        Navigator.of(context).pop();
      },
    );
    
    Widget continueButton = FlatButton(
      child: Text(isAll ? "Entrar" : "Sair"),
      onPressed: () async {
        if (isAll) {
          groupManager.enterInGroup(
            group: group,
            onFail: () {
              scaffoldKey.currentState.showSnackBar(
                SnackBar(
                  backgroundColor: Colors.redAccent,
                  content: Text('Erro!'),
                ),
              );
              Navigator.of(context).pop();
            },
            onSuccess: () {
              scaffoldKey.currentState.showSnackBar(
                SnackBar(
                  backgroundColor: Colors.blueAccent,
                  content: Text('Sucesso!'),
                ),
              );
              Navigator.of(context).pop();
            },
          );
        } else {
          // TODO : Função para sair do Grupo
          Navigator.of(context).pop();
        }
      },
    );

    // set up the AlertDialog
    AlertDialog alert = AlertDialog(
      title: Text(isAll ?"Entrar no Grupo": "Informações do Grupo"),
      content: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        children: [
          Text(
              "De: ${group.origem}, ${group.destino}\nPara: ${group.destino}. ${group.enderecoDestino}"),
          Text("Saida ${df.format(group.horario)}"),
        ],
      ),
      actions: [
        cancelButton,
        continueButton,
      ],
    );

    // show the dialog
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: isAll ? groupsManager.getGroups() : groupsManager.getMyGroups(),
      builder: (BuildContext context, AsyncSnapshot snapshot) {
        if (snapshot.connectionState != ConnectionState.done) {
          return Center(child: CircularProgressIndicator());
        } else {
          if (groupsManager.groups.isNotEmpty) {
            return RefreshIndicator(
              onRefresh: () async {
                await groupsManager.getGroups();
              },
              child: Scrollbar(
                child: ListView.builder(
                  physics: BouncingScrollPhysics(
                      parent: AlwaysScrollableScrollPhysics()),
                  itemCount: groupsManager.groups.length,
                  itemBuilder: (context, cont) {
                    var group = groupsManager.groups[cont];
                    return GestureDetector(
                      onTap: () {
                        showAlertDialog(context, groupsManager, group);
                      },
                      child: GroupCard(
                        group: group,
                      ),
                    );
                  },
                ),
              ),
            );
          } else {
            return Center(
              child: Text("Vazio"),
            );
          }
        }
      },
    );
  }
}
