import 'package:flutter/material.dart';
import 'package:campuswaymobile/providers/user_manager.dart';
import 'package:provider/provider.dart';

bool emailValidator(String email) {
  final RegExp regex = RegExp(
      r"^(([^<>()[\]\\.,;:\s@\']+(\.[^<>()[\]\\.,;:\s@\']+)*)|(\'.+\'))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$");
  return regex.hasMatch(email);
}

class RegisterScreen extends StatelessWidget {
  final TextEditingController emailController = TextEditingController();
  final TextEditingController passwordController = TextEditingController();
  final TextEditingController password_verifierController =
      TextEditingController();
  final TextEditingController usernameController = TextEditingController();
  final TextEditingController phoneController = TextEditingController();
  final TextEditingController firstNameController = TextEditingController();
  final TextEditingController lastNameController = TextEditingController();
  final TextEditingController courseController = TextEditingController();
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  /*
  DADOS EXEMPLO
  {
        "username": "gabrielseabra2",
        "password":"biscoito",
        "email":"gseabra@gmail.com",
        "phone":"23452323",
        "first_name":"Gabriel",
        "last_name": "Seabra",
        "course":"Engenharia da Computação"
    }
  */

  @override
  Widget build(BuildContext context) {
    return Consumer<UserManager>(builder: (context, userManager, child) {
      return Scaffold(
        key: _scaffoldKey,
        backgroundColor: Theme.of(context).primaryColor,
        body: Center(
          child: SingleChildScrollView(
            child: Card(
              elevation: 6,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(15.0),
              ),
              margin: EdgeInsets.all(13),
              child: Form(
                key: _formKey,
                child: ListView(
                  padding: EdgeInsets.all(18),
                  shrinkWrap: true,
                  children: [
                    Container(
                      width: MediaQuery.of(context).size.width / 5.6,
                      height: MediaQuery.of(context).size.width / 5.6,
                      padding: EdgeInsets.all(20),
                      alignment: Alignment.center,
                      decoration: BoxDecoration(
                        image: DecorationImage(
                          image:
                              AssetImage('assets/images/logo-background.png'),
                          fit: BoxFit.scaleDown,
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 15,
                    ),
                    TextFormField(
                      enabled: !userManager.loading,
                      controller: emailController,
                      autocorrect: false,
                      keyboardType: TextInputType.emailAddress,
                      decoration: InputDecoration(
                        contentPadding: EdgeInsets.symmetric(
                            vertical: 2.0, horizontal: 17.0),
                        hintText: "Email",
                        fillColor: Colors.white,
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(25.0),
                          borderSide: BorderSide(),
                        ),
                      ),
                      validator: (email) {
                        if (email.isEmpty)
                          return "Email não pode ser vazio";
                        else if (!emailValidator(email))
                          return "Email inválido";
                        else
                          return null;
                      },
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    TextFormField(
                      enabled: !userManager.loading,
                      controller: usernameController,
                      autocorrect: false,
                      decoration: InputDecoration(
                        contentPadding: EdgeInsets.symmetric(
                            vertical: 2.0, horizontal: 17.0),
                        hintText: "Nome de usuário",
                        fillColor: Colors.white,
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(25.0),
                          borderSide: BorderSide(),
                        ),
                      ),
                      validator: (username) {
                        if (username.isEmpty)
                          return "Nome de usuário não pode ser vazio";
                        else
                          return null;
                      },
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    TextFormField(
                      enabled: !userManager.loading,
                      controller: firstNameController,
                      autocorrect: false,
                      keyboardType: TextInputType.name,
                      decoration: InputDecoration(
                        contentPadding: EdgeInsets.symmetric(
                            vertical: 2.0, horizontal: 17.0),
                        hintText: "Nome",
                        fillColor: Colors.white,
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(25.0),
                          borderSide: BorderSide(),
                        ),
                      ),
                      validator: (first_name) {
                        if (first_name.isEmpty)
                          return "Nome não pode ser vazio";
                        else
                          return null;
                      },
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    TextFormField(
                      enabled: !userManager.loading,
                      controller: lastNameController,
                      autocorrect: false,
                      keyboardType: TextInputType.name,
                      decoration: InputDecoration(
                        contentPadding: EdgeInsets.symmetric(
                            vertical: 2.0, horizontal: 17.0),
                        hintText: "Sobrenome",
                        fillColor: Colors.white,
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(25.0),
                          borderSide: BorderSide(),
                        ),
                      ),
                      validator: (last_name) {
                        if (last_name.isEmpty)
                          return "Sobrenome não pode ser vazio";
                        else
                          return null;
                      },
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    TextFormField(
                      enabled: !userManager.loading,
                      controller: phoneController,
                      autocorrect: false,
                      keyboardType: TextInputType.phone,
                      decoration: InputDecoration(
                        contentPadding: EdgeInsets.symmetric(
                            vertical: 2.0, horizontal: 17.0),
                        hintText: "Telefone",
                        fillColor: Colors.white,
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(25.0),
                          borderSide: BorderSide(),
                        ),
                      ),
                      validator: (phone) {
                        if (phone.isEmpty)
                          return "Telefone não pode ser vazio";
                        else
                          return null;
                      },
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    TextFormField(
                      enabled: !userManager.loading,
                      controller: courseController,
                      autocorrect: false,
                      keyboardType: TextInputType.name,
                      decoration: InputDecoration(
                        contentPadding: EdgeInsets.symmetric(
                            vertical: 2.0, horizontal: 17.0),
                        hintText: "Curso",
                        fillColor: Colors.white,
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(25.0),
                          borderSide: BorderSide(),
                        ),
                      ),
                      validator: (course) {
                        if (course.isEmpty)
                          return "Curso não pode ser vazio";
                        else
                          return null;
                      },
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    TextFormField(
                      enabled: !userManager.loading,
                      controller: passwordController,
                      decoration: InputDecoration(
                        contentPadding: EdgeInsets.symmetric(
                            vertical: 2.0, horizontal: 17.0),
                        hintText: "Senha",
                        fillColor: Colors.white,
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(25.0),
                        ),
                      ),
                      obscureText: true,
                      autocorrect: false,
                      validator: (password) {
                        if (password.isEmpty)
                          return "Senha não pode ser vazia";
                        else if (password.length < 6)
                          return "Senha muito pequena";
                        else
                          return null;
                      },
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    TextFormField(
                      enabled: !userManager.loading,
                      controller: password_verifierController,
                      decoration: InputDecoration(
                        contentPadding: EdgeInsets.symmetric(
                            vertical: 2.0, horizontal: 17.0),
                        hintText: "Digite sua senha novamente",
                        fillColor: Colors.white,
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(25.0),
                        ),
                      ),
                      obscureText: true,
                      autocorrect: false,
                      validator: (password_verifier) {
                        if (password_verifier != passwordController.text)
                          return "As senhas digitadas devem ser iguais";
                        else
                          return null;
                      },
                    ),
                    SizedBox(
                      height: 15,
                    ),
                    ClipRRect(
                      borderRadius: BorderRadius.all(Radius.circular(25)),
                      child: Container(
                        height: 45,
                        child: RaisedButton(
                          onPressed: !userManager.loading
                              ? () {
                                  if (_formKey.currentState.validate()) {
                                    userManager.register(
                                        login: usernameController.text,
                                        password: passwordController.text,
                                        course: courseController.text,
                                        email: emailController.text,
                                        firstName: firstNameController.text,
                                        lastName: lastNameController.text,
                                        phone: phoneController.text,
                                        onFail: (error) {
                                          _scaffoldKey.currentState
                                              .showSnackBar(
                                            SnackBar(
                                              backgroundColor: Colors.redAccent,
                                              content: Text(
                                                  'Erro no cadastro: $error'),
                                            ),
                                          );
                                        },
                                        onSuccess: () => {
                                              Navigator.of(context)
                                                  .pushReplacementNamed('/base')
                                            });
                                  }
                                }
                              : null,
                          color: Theme.of(context).accentColor,
                          disabledColor:
                              Theme.of(context).accentColor.withAlpha(100),
                          textColor: Colors.white,
                          child: Text(
                            "Entrar",
                            style: TextStyle(fontSize: 16),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
      );
    });
  }
}
