import 'package:campuswaymobile/sceens/home_page/home_page.dart';
import 'package:campuswaymobile/sceens/settings_screen/settings_screen.dart';
import 'package:flutter/material.dart';

class BaseScreen extends StatefulWidget {
  @override
  _BaseScreenState createState() => _BaseScreenState();
}

class _BaseScreenState extends State<BaseScreen> {
  int selectedIndex = 0;
  List<Widget> _children;
  final pageController = PageController(initialPage: 0);

  void onPageChanged(int index) {
    setState(() {
      selectedIndex = index;
    });
  }

  @override
  void initState() {
    super.initState();
    _children = [
      MyHomePage(),
      SettingsScreeen(),
    ];
  }

  @override
  Widget build(BuildContext context) {
    
    return WillPopScope(
      onWillPop: () async => false,
      child: Scaffold(
        resizeToAvoidBottomPadding: false,
        body: PageView(
          physics: NeverScrollableScrollPhysics(),
          children: _children,
          controller: pageController,
          onPageChanged: onPageChanged,
        ),
        bottomNavigationBar: BottomNavigationBar(
          elevation: 25,
          currentIndex: selectedIndex,
          onTap: (index) {
            pageController.jumpToPage(index);
          },
          items: [
            BottomNavigationBarItem(
              icon: Icon(Icons.folder),
              label: "Grupos",
            ),
            
            BottomNavigationBarItem(
              icon: Icon(Icons.settings),
              label: "Ajustes",
            ),
          ],
        ),
      ),
    );
  }
}