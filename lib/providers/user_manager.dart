import 'dart:convert';
import 'package:campuswaymobile/helpers/shared_preferences/shared_preferences_helper.dart';
import 'package:http/http.dart' as http;
import 'package:campuswaymobile/models/user.dart';
import 'package:campuswaymobile/utilities/enviroments.dart';
import 'package:flutter/foundation.dart';
import 'package:shared_preferences/shared_preferences.dart';

class UserManager extends ChangeNotifier {
  //Uma instância dessa classe estará alocada na raíz
  //da árvore de Widgets da aplicação, podendo ser acessada
  //em qualquer widget.

  User user;
  SharedPreferenceHelper preferences =
      SharedPreferenceHelper(SharedPreferences.getInstance());
  final baseUrl = Enviroment.apiUrl;
  static const headers = {"Content-Type": "application/json"};

  bool loading = false;

  UserManager() {
    getUser();
  }
  
  getUser() async {
    if (await preferences.isLoggedIn) {
      user = await preferences.user;
    }
  }

  Future<void> login(
      {String login,
      String password,
      Function onFail,
      Function onSuccess}) async {
    setLoading(true);

    try {
      var response = await http.post(
        baseUrl + 'login/',
        headers: headers,
        body: jsonEncode({"username": login, "password": password}),
      );

      if (response.statusCode == 200) {
        var jsonBody = json.decode(utf8.decode(response.bodyBytes));
        user = User.fromJson(jsonBody);
        preferences.logIn(user); // save data in LocalStorage
        onSuccess();
      } else if (response.statusCode == 400) {
        onFail("Usuário e/ou senha incorretos.");
      } else {
        onFail(json.decode(utf8.decode(response.bodyBytes)));
      }
    } catch (e) {
      onFail(e);
    }
    setLoading(false);
  }

  Future<void> register(
      {String login,
      String password,
      String email,
      String phone,
      String course,
      String firstName,
      String lastName,
      Function onFail,
      Function onSuccess}) async {
    setLoading(true);

    try {
      var response = await http.post(
        baseUrl + 'users/register/',
        headers: headers,
        body: jsonEncode({
          "username": login,
          "password": password,
          "email": email,
          "phone": phone,
          "course": course,
          "first_name": firstName,
          "last_name": lastName,
        }),
      );

      if (response.statusCode == 200 || response.statusCode == 201) {
        var jsonBody = json.decode(utf8.decode(response.bodyBytes));
        user = User.fromJson(jsonBody);
        preferences.logIn(user); // save data in LocalStorage
        onSuccess();
      } else if (response.statusCode == 400) {
        onFail("Usuário e/ou senha incorretos.");
      } else {
        onFail(json.decode(utf8.decode(response.bodyBytes)));
      }
    } catch (e) {
      onFail(e);
    }
    setLoading(false);
  }

  //TODO
  Future<void> logout() async {}

  void setLoading(bool value) {
    loading = value;
    notifyListeners();
  }
}
