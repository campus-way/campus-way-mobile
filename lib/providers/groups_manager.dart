import 'dart:convert';

import 'package:campuswaymobile/helpers/shared_preferences/shared_preferences_helper.dart';
import 'package:campuswaymobile/models/group.dart';
import 'package:campuswaymobile/utilities/enviroments.dart';
import 'package:http/http.dart' as http;
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class GroupsManager extends ChangeNotifier {
  List<Group> groups = [];

  SharedPreferenceHelper preferences =
      SharedPreferenceHelper(SharedPreferences.getInstance());
  final baseUrl = Enviroment.apiUrl;

  Future<bool> getGroups() async {
    String token = await preferences.authToken;

    var headers = {"Content-Type": "application/json", "Authorization": token};

    var response = await http.get(
      baseUrl + 'groups/',
      headers: headers,
    );

    if (response.statusCode == 200) {
      var jsonBody = json.decode(utf8.decode(response.bodyBytes));
      List<Group> groupList = [];
      for (var gp in jsonBody) {
        groupList.add(Group.fromJson(gp));
      }
      groups = groupList;
      return true;
    } else {
      /// handle error
      return false;
    }
  }
  Future<bool> getMyGroups() async {
    String token = await preferences.authToken;

    var headers = {"Content-Type": "application/json", "Authorization": token};

    var response = await http.get(
      baseUrl + 'groups/my_groups/',
      headers: headers,
    );

    if (response.statusCode == 200) {
      var jsonBody = json.decode(utf8.decode(response.bodyBytes));
      List<Group> groupList = [];
      for (var gp in jsonBody) {
        groupList.add(Group.fromJson(gp));
      }
      groups = groupList;
      return true;
    } else {
      /// handle error
      return false;
    }
  }

  Future<void> createGroup({
    Group group,
    Function onSuccess,
    Function onFail,
  }) async {
    String token = await preferences.authToken;
    group.authorId = await preferences.id;

    var headers = {"Content-Type": "application/json", "Authorization": token};

    print(group.toJson());
    var response = await http.post(baseUrl + 'groups/',
        headers: headers, body: jsonEncode(group.toJson()));

    if (response.statusCode == 201) {
      var jsonBody = json.decode(utf8.decode(response.bodyBytes));
      this.groups.add(Group.fromJson(jsonBody));
      onSuccess();
    } else if (response.statusCode == 500) {
      onFail("Erro no Servidor.");
    } else if (response.statusCode == 400) {
      onFail("Erro no envio dos dados.");
    } else {
      onFail("Erro desconhecido.");
    }
  }

  Future<void> enterInGroup({
    Group group,
    Function onSuccess,
    Function onFail,
  }) async {
    String token = await preferences.authToken;
    var id = await preferences.id;

    var headers = {"Content-Type": "application/json", "Authorization": token};

    print(group.toJson());
    var response = await http.post(baseUrl + 'groups/${group.id}/members/',
        headers: headers);

    if (response.statusCode == 200) {
      var jsonBody = json.decode(utf8.decode(response.bodyBytes));
      print(jsonBody);
      onSuccess();
    } else if (response.statusCode == 500) {
      onFail("Erro no Servidor.");
    } else if (response.statusCode == 400) {
      onFail("Erro no envio dos dados.");
    } else {
      onFail("Erro desconhecido.");
    }
  }
}
