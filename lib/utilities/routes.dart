class RouteNames{
  static const login = '/login';
  static const register = '/register';
  static const home = '/home';
  static const base = '/base';
  static const groupForm = '/group_form';
}