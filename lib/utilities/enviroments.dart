class Enviroment {
  
  // No caso, como estava executando no emulador, 
  // devo rodar a API no endereço de IP da Máquina onde o Django é executado, não no localhost.
  // (para encontrar o IP é só acessar o terminal e digitar 'ipconfig'), 
  // No caso do Django, para fazer isso, basta usar o comando: 'python manage.py runserver 0.0.0.0:8000'
   
  // Mudar para endereço da API
  static final apiUrl = "http://192.168.0.105:8000/api/"; 

  static final production =  false;
}
