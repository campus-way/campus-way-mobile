class Group {
  int id;
  int authorId;
  String origem;
  String destino;
  String enderecoOrigem;
  String enderecoDestino;
  String pontoReferencia;
  DateTime horario;
  int participantes;

  Group(
      {this.id,
      this.origem,
      this.destino,
      this.enderecoOrigem,
      this.enderecoDestino,
      this.pontoReferencia,
      this.horario});

  Group.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    origem = json['origem'];
    authorId = json['autor'];
    destino = json['destino'];
    enderecoOrigem = json['endereco_origem'];
    enderecoDestino = json['endereco_destino'];
    pontoReferencia = json['ponto_referencia'];
    participantes = json['members_number'];
    horario = DateTime.parse(json['horario']);
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['origem'] = this.origem;
    data['autor'] = this.authorId;
    data['destino'] = this.destino;
    data['endereco_origem'] = this.enderecoOrigem;
    data['endereco_destino'] = this.enderecoDestino;
    data['ponto_referencia'] = this.pontoReferencia;
    data['horario'] = this.horario.toString();
    return data;
  }
}