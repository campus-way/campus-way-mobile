class User {
  String username;
  String email;
  String firstName;
  String lastName;
  String phone;
  String token;
  int id;
  String course;

  //constructor
  User({
    this.username,
    this.email,
    this.firstName,
    this.lastName,
    this.phone,
    this.course,
    this.id,
    this.token,
  });

  User.fromJson(Map<String, dynamic> json){
    username = json['username'];
    email = json['email'];
    phone = json['phone'];
    firstName = json['first_name'];
    lastName = json['last_name'];
    course = json['course'];
    id = json['id'];
    token = json['token'];
  }


  /*
  DADOS EXEMPLO
  {
        "username": "gabrielseabra2",
        "password":"biscoito",
        "email":"gseabra@gmail.com",
        "phone":"23452323",
        "first_name":"Gabriel",
        "last_name": "Seabra",
        "course":"Engenharia da Computação"
    }
  */
}
