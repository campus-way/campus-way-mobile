import 'package:campuswaymobile/helpers/shared_preferences/dtos.dart';
import 'package:campuswaymobile/providers/groups_manager.dart';
import 'package:campuswaymobile/providers/user_manager.dart';
import 'package:campuswaymobile/sceens/base_screen/base_screen.dart';
import 'package:campuswaymobile/sceens/home_page/components/new_group_form.dart';
import 'package:campuswaymobile/sceens/home_page/home_page.dart';
import 'package:campuswaymobile/sceens/login/login_screen.dart';
import 'package:campuswaymobile/sceens/register/register_screen.dart';
import 'package:campuswaymobile/utilities/routes.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'helpers/shared_preferences/preferences.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  SharedPreferences preferences = await SharedPreferences.getInstance();
  runApp(MyApp(prefs: preferences));
}

class MyApp extends StatelessWidget {
  final prefs;

  const MyApp({Key key, this.prefs}) : super(key: key);
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    // como explicado em 'providers/user_manager.dart', temos na raíz da
    // nossa arvore de widgets os providers
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);
    return MultiProvider(
      providers: [
        //Adicionamos a essa lista os providers
        ChangeNotifierProvider(
          create: (_) => UserManager(),
          lazy: false,
        ),
        ChangeNotifierProvider(
          create: (_) => GroupsManager(),
          lazy: false,
        ),
      ],
      child: MaterialApp(
        title: 'Campus Way',
        debugShowCheckedModeBanner: false,
        theme: ThemeData(
          primaryColor: Color.fromRGBO(176, 80, 144, 1),
          accentColor: Color.fromRGBO(79, 192, 207, 1),
          visualDensity: VisualDensity.adaptivePlatformDensity,
        ),
        initialRoute: prefs.getBool(Preferences.is_logged_in) != null
            ? RouteNames.base
            : RouteNames.login,
        onGenerateRoute: (settings) {
          switch (settings.name) {
            case RouteNames.home:
              return MaterialPageRoute(
                builder: (_) => MyHomePage(),
              );
            case RouteNames.login:
              return MaterialPageRoute(
                builder: (_) => LoginScreen(),
              );
            case RouteNames.register:
              return MaterialPageRoute(
                builder: (_) => RegisterScreen(),
              );
            case RouteNames.base:
              return MaterialPageRoute(
                builder: (_) => BaseScreen(),
              );
            case RouteNames.groupForm:
              return MaterialPageRoute(
                builder: (_) => GroupForm(
                  groupsManager:
                      (settings.arguments as FormsParams).groupsManager,
                  onSuccessCallback:
                      (settings.arguments as FormsParams).callBack,
                ),
              );

            default:
              return prefs.getBool(Preferences.is_logged_in) != null
                  ? MaterialPageRoute(
                      builder: (_) => BaseScreen(),
                    )
                  : MaterialPageRoute(
                      builder: (_) => LoginScreen(),
                    );
          }
        },
      ),
    );
  }
}
