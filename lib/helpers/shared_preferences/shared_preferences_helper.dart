import 'dart:async';
import 'package:campuswaymobile/helpers/shared_preferences/preferences.dart';
import 'package:campuswaymobile/models/user.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SharedPreferenceHelper {
  // shared pref instance
  final Future<SharedPreferences> _sharedPreference;

  // constructor
  SharedPreferenceHelper(this._sharedPreference);

  // General Methods: ----------------------------------------------------------

  // Getters
  Future<dynamic> get authToken async {
    return _sharedPreference.then((preference) {
      return preference.getString(Preferences.auth_token);
    });
  }

  Future<int> get id async {
    return _sharedPreference.then((preference) {
      return preference.getInt(Preferences.id);
    });
  }

  Future<String> get loginName async {
    return _sharedPreference.then((preference) {
      return preference.getString(Preferences.login);
    });
  }

  Future<String> get email async {
    return _sharedPreference.then((preference) {
      return preference.getString(Preferences.email);
    });
  }

  Future<String> get firstName async {
    return _sharedPreference.then((preference) {
      return preference.getString(Preferences.firstName);
    });
  }

  Future<String> get lastName async {
    return _sharedPreference.then((preference) {
      return preference.getString(Preferences.lastName);
    });
  }

  Future<User> get user async {
    return _sharedPreference.then((preference) async {
      return User(
        email: await this.email,
        username: await this.loginName,
        token: await this.authToken,
        id: await this.id,
        firstName: await this.firstName,
        lastName: await this.lastName,        
      );
    });
  }

  // Setters
  Future<void> saveAuthToken(String authToken) async {
    return _sharedPreference.then((preference) {
      preference.setString(Preferences.auth_token, authToken);
    });
  }

  Future<void> saveLoginName(String login) async {
    return _sharedPreference.then((preference) {
      preference.setString(Preferences.login, login);
    });
  }

  Future<void> saveEmail(String email) async {
    return _sharedPreference.then((preference) {
      preference.setString(Preferences.email, email);
    });
  }

  Future<void> saveId(int id) async {
    return _sharedPreference.then((preference) {
      preference.setInt(Preferences.id, id);
    });
  }

  Future<void> saveIsLoggedIn(bool remember) async {
    return _sharedPreference.then((preference) {
      if (remember) preference.setBool(Preferences.is_logged_in, true);
    });
  }

  Future<void> saveFirstName(String fName) async {
    return _sharedPreference.then((preference) {
      preference.setString(Preferences.firstName, fName);
    });
  }

  Future<void> saveLastName(String lName) async {
    return _sharedPreference.then((preference) {
      preference.setString(Preferences.lastName, lName);
    });
  }

  Future<void> logOut() async {
    return _sharedPreference.then((preference) {
      preference.remove(Preferences.auth_token);
      preference.remove(Preferences.login);
      preference.remove(Preferences.email);
      preference.remove(Preferences.id);
      preference.remove(Preferences.firstName);
      preference.remove(Preferences.lastName);
      preference.remove(Preferences.is_logged_in);
    });
  }

  Future<void> logIn(User user) async {
    return _sharedPreference.then((preference) {
      saveAuthToken(user.token);
      saveEmail(user.email);
      saveLoginName(user.username);
      saveFirstName(user.firstName);
      saveLastName(user.lastName);
      saveId(user.id);
      saveIsLoggedIn(true);
    });
  }

  Future<bool> get isLoggedIn async {
    return _sharedPreference.then((preference) {
      return preference.getBool(Preferences.is_logged_in) != true
          ? false
          : true;
    });
  }
}
