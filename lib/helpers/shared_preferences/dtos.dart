import 'package:campuswaymobile/providers/groups_manager.dart';

class FormsParams {
  GroupsManager groupsManager;
  Function callBack;
  FormsParams({this.callBack,this.groupsManager});
}