class Preferences {
  Preferences._();

  static const String is_logged_in = "isLoggedIn";
  static const String auth_token = "authToken";
  static const String login = "login";
  static const String email = "email";
  static const String id = "id";
  static const String firstName = "first_name";
  static const String lastName = "last_name";
  static const String course = "course";
  
}