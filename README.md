# Campus Way

O Campus Way é um aplicativo que traz maior segurança para os alunos da Unicamp. 
Todos que já tiveram a oportunidade de morar por Barão Geraldo sabem o quão perigoso pode ser andar pelo bairro. Especialmente à noite, as ruas de Barão ficam muito vazias e escuras, o que favorece a ocorrência de assaltos ou até estupros. É muito difícil para um aluno da Unicamp ter que passar aperto ou medo toda vez que volta sozinho das aulas à noite. Nesse sentido, o CampusWay fornece a possibilidade de um aluno encontrar outras pessoas que vão fazer um trajeto parecido até o seu destino. Assim, os alunos agora podem andar por Barão em grupos, aumentando o sentimento de segurança no trajeto. 

Vídeo Explicação do Projeto:
https://youtu.be/sAe8eYXFTUc

ATENÇÃO:
USAR BAIXAR A SDK DO FLUTTER VERSÃO 1.22.5
E DART VERSÃO 2.10.4

## Getting Started

This project is a starting point for a Flutter application.

A few resources to get you started if this is your first Flutter project:

- [Lab: Write your first Flutter app](https://flutter.dev/docs/get-started/codelab)
- [Cookbook: Useful Flutter samples](https://flutter.dev/docs/cookbook)

For help getting started with Flutter, view our
[online documentation](https://flutter.dev/docs), which offers tutorials,
samples, guidance on mobile development, and a full API reference.

## Descrição da Arquitetura

Diagrama C4:

![Diagrama C4](img/Avaliacao5.jpeg)

#### Breve Descrição dos Componentes

###### Componentes padrão do projeto django rest:

- URL Patterns (urls.py): realiza o processamento das urls requisitadas e faz o direcionamento para as respectivas respostas do servidor (views).

- Views (views.py): efetivam as ações relacionadas à funcionalidade particular do sistema construído, onde se implementa a lógica de negócio específica do projeto bem como a comunicação com os modelos (models).

- Models (models.py): fazem a definição dos modelos de dados a serem utilizados sem a necessidade de escrita em SQL explícito e realizam a comunicação direta com as tabelas do BD.

###### Componentes específicos:

- authentication: provê sistema de authenticação de usuários, incluindo cadastro e validação de login. Além disso, faz as definições do modelo de usuário utilizado (campos armazenados) bem como realiza a comunicação com o banco de dados para alteração e recuperação de dados da tabela de usuários.

- authentication/api: implementa a interface da rest api, realizando as transações (recebimento e envio) de dados dos usuários em formato json.

- group: implementa a funcionalidade do serviço, fazendo a definição dos modelos dos "grupos" (meio pelo qual os usuários irão se agrupar para as caminhadas) e efetivando as transações com as tabelas do banco de dados relacionadas.

###### Componente Mobile app:
Componente responsável pela interação do usuário em seu dispositivo móvel. 
Este componente é dividido em diferentes telas, para melhor utilização do usuário.

- login_screen: usuário se autentica, para que possa proceder a tela principal do aplicativo, a home_page. Além disso o usuário possui a opção de prosseguir para a tela de registro.

- register_screen: tela de cadastro do usuário. Em caso de sucesso no registro do novo usuário, ele é redirecionado para a home_page

- home_page: Tela principal do aplicativo, usuário pode registrar um novo grupo no banco de dados, e pode ir para sua página de perfil, a profile_page

- profile_page (sprint 3): Tela de perfil do usuário, onde pode controlar e alterar suas informações.

#### Padrão de Projeto

Para o componente *Aplicativo Móvel* do projeto CampusWay, implementamos o padrão de projeto Abstract Factory para resolver o seguinte problema:Por mais que estejamos usando a mesma base de código com o Flutter, geralmente há uma demanda de que alguns componentes da IU devem ter uma aparência diferente em plataformas diferentes. O caso de uso mais simples imaginável no contexto Flutter - mostrando os widgets de estilo Material ou Cupertino com base em se você está usando um dispositivo Android ou iOS. 

No caso da Avaliação 5.1, implementamos o padrão na utilização de um Dialog com uma mensagem, mas podemos implementar para qualquer widget que pode ser diferente nas plataformas Android e iOS, como um Switch ou um Botão. Para isso, basta definir um método na interface *IWidgetsFactory* e implementá-lo nas classes *MaterialWidgetsFactory* e *CupertinoWidgetsFactory*, definindo também uma interface para tal componente.

![Diagrama C4](img/abstract-factory.jpeg)
